<?php
if($_SESSION['logined']!='cookies.exout.net'){
	header('HTTP/1.1 403 Forbidden', true, 403);
	print <<<XML
<?xml version="1.0"?>
	<result>
		<error>
			<errmsg>You don\'t have permission.</errmsg>
		</error>
	</result>
XML;
}

$xml = new SimpleXMLElement(); 

  $character = $xml->book[0]->characters->addChild('character'); 
  $character->addChild('name', 'Yellow Cat'); 
  $character->addChild('desc', 'aloof'); 

  $success = $xml->book[0]->addChild('success', '2'); 
  $success->addAttribute('type', 'reprints'); 

  echo $xml->asXML();
  ?>