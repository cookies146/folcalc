<?php
require_once('./func/operation.inc');
require_once('./func/term.inc');
require_once('./func/stack.inc');

$statuses = array();

function calc_init($expression){
	if(checkExpression($expression)==0){
		return false;
	}
	return true;
}

function calc_exec($connection, $expression){
	global $operation_operations_pattern, $operation_term_pattern;
	if(!calc_init($expression)){
		return false;
	}
	$rpn = IN2RPN($expression);
	preg_match_all("/{$operation_term_pattern}/", $rpn, $terms);
	foreach($terms[0] as $value){
		load_term($connection, $value);
	}
	
	$calc_stack = new stack();
	while(preg_match('/^[^ ]+/',$rpn, $matches)){
		if(preg_match('/^'.$operation_term_pattern.'/', $matches[0])){
			calc_term($calc_stack, $matches[0]);
		}elseif(preg_match('/^'.$operation_operations_pattern.'/', $matches[0])){
			calc_operation($calc_stack, $matches[0]);
		}
		$rpn = preg_replace('/^'.str_replace('/','\/',$matches[0])." /", "", $rpn);
	}
return $calc_stack->pop();
}
			
function split_term($term, &$scrname, &$follow){
	$matches = explode('/', $term);
	$scrname = $matches[0];
	$follow = $matches[1];
return true;
}

function load_term($connection, $term){
	global $statuses;
	split_term($term, $scrname, $follow);
	$res = termExec($connection, $scrname, $follow);
	if(!empty($statuses[$follow][$scrname])){
		return true;
	}
	if($res!=false){
		$statuses[$follow][$scrname] = $res;
		return true;
	}
return false;
}

function calc_term($stack, $term){
	global $statuses;
	split_term($term, $scrname, $follow);
	$stack->push($statuses[$follow][$scrname]);
return true;
}

function calc_operation($stack, $operation){
	if($operation=='AND'||$operation=='&'){
		$res = oprAND($stack->pop(), $stack->pop());
	}elseif($operation=='OR'||$operation=='|'){
		$res = oprOR($stack->pop(), $stack->pop());
	}elseif($operation=='XOR'||$operation=='^'){
		$res = oprXOR($stack->pop(), $stack->pop());
	}
	$stack->push($res);
return true;
}