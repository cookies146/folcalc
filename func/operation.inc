<?php

$operation_term_pattern = '@[a-z0-9_]{1,15}\/(following|followers)';
$operation_operations_pattern = '(AND|&|OR|\||XOR|\^)';
$operation_expression_pattern = "/^ *\(? *{$operation_term_pattern}( *{$operation_operations_pattern} *\(? *{$operation_term_pattern} *\)?)*$/";

require_once('stack.inc');

function oprAND($array1, $array2){
	return array_intersect_key($array1, $array2);
}

function oprOR($array1, $array2){
	return $array1 + $array2;
}

function oprXOR($array1, $array2){
	return array_diff_key($array1, $array2) + array_diff_key($array2, $array1);
}

function checkExpression($expression){
	global $operation_expression_pattern;
	return (preg_match($operation_expression_pattern, $expression) && substr_count($expression, '(')==substr_count($expression, ')'));
}

function IN2RPN($expression){
	global $operation_term_pattern, $operation_operations_pattern;
	$expression = str_replace(' ', '', $expression);
	$result = '';
	
	$opr_stk = new stack();
	$opr_level_stk = new stack();
	$matches = array(); $stk_level=0;$level=0;
	
	while($expression!=""){
		$ss = substr($expression, 0, 1);
		if($ss=='('){
			$level++;
			$expression = substr($expression, 1);
			continue;
		}elseif($ss==')'){
			$level--;
			if($level<0){
				return flase;
			}
			$expression = substr($expression, 1);
			continue;
		}elseif(preg_match('/^'.$operation_term_pattern.'/', $expression, $matches)){
			$result .= $matches[0] . ' ';
		}elseif(preg_match('/^'.$operation_operations_pattern.'/', $expression, $matches)){
			$stacksize = $opr_stk->size();
			if(($stacksize=0)||($stk_level<$level)){
				$opr_stk->push($matches[0]);
				$opr_level_stk->push($level);
			}elseif($opr_level_stk->peek()>=$level){
				while(($opr_level_stk->peek()>=$level)&&($opr_stk->size()!=0)){
					$opr_level_stk->pop();
					$result .= $opr_stk->pop() . ' ';
				}
				$opr_stk->push($matches[0]);
				$opr_level_stk->push($level);
			}
		}else{
			return false;
		}
		$pattern = "/^".str_replace('/', '\/', $matches[0]) . "/";
		$expression= preg_replace($pattern, "", $expression);
	}
	for($i=0,$size=$opr_stk->size();$i<$size;$i++){
		$result .= $opr_stk->pop() . ' ';
	}
return $result;
}
?>
