<?php
class stack
{
	private $stack;
	
	public function __construct(){
		$this->stack = array();
	}
	
	public function __destruct(){
		$this->stack = array();
		unset($this->stack);
	}
	
	public function push($item){
		if(array_push($this->stack, $item)){return true;}
		return false;
	}
	
	public function pop(){
		return array_pop($this->stack);
	}
	
	public function peek($position=-1) { // Get certain element on the stack
		$elements = count($this->stack);
		if($elements == 0){
			return 0;
		}
		if($position==-1){
			$position=$elements-1;
		}
		if ($elements < $position) {
			unset($elements);
			return FALSE;
		} else {
			unset($elements);
			return $this->stack[$position];
		}
	}
	
	public function size(){
		return count($this->stack);
	}
}
?>