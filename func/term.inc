<?php
function termExec($connection, $screen_name, $type){
	if($type=='following'){
		return termFollowing($connection, 'screen_name', $screen_name);
	}elseif($type=='followers'){
		return termFollowers($connection, 'screen_name', $screen_name);
	}
return false;
}
		
function termFollowing($connection, $type, $value){
	$param = Array($type=>$value);
	$content = array();$next_cursor='-1';
	while($next_cursor!='0'){
		$param['cursor'] = sprintf('%.0f',$next_cursor);
		$tmp = $connection->get('statuses/friends',$param);
		$next_cursor = sprintf('%.0f',$tmp->next_cursor_str);
		$content = array_merge($content, $tmp->users);
	}
	$result=array();
	foreach($content as $value){
		$result[sprintf("%.0f", $value->id)]=$value;
	}
	return $result;
}

function termFollowers($connection, $type, $value){
	$param = Array($type=>$value);
	$content = array();$next_cursor='-1';
	while($next_cursor!='0'){
		$param['cursor'] = sprintf('%.0f',$next_cursor);
		$tmp = $connection->get('statuses/followers',$param);
		$next_cursor = sprintf('%.0f',$tmp->next_cursor_str);
		$content = array_merge($content, $tmp->users);
	}
	$result=array();
	foreach($content as $value){
		$result[sprintf("%.0f", $value->id)]=$value;
	}
	return $result;
}

?>