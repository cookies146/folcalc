<?php
set_time_limit(180);
/* Load required lib files. */
session_start();
require_once('../twitteroauth/twitteroauth.php');
require_once('../config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ../clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

$content = $connection->get('help/test');
if($content!="ok"){
	if($connection->http_code!=200){
		die("Connection Error:".__LINE__.$connection->http_code);
	}else{
		die("Unknown Error.".__LINE__);
	}
}
/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/rate_limit_status');
print_r($content);
echo "Current API hits remaining: {$content->remaining_hits}.<br>";

/* Get logged in user to help with tests. */
$user = $connection->get('account/verify_credentials');
echo ;
/* If method is set change API call made. Test is called by default. */

echo "<table border='1'>";
echo "<th></th>";
require_once("./calculate.inc");
$res=calc_exec($connection, "@k5342/following AND @k5342/following");
foreach($res as $value){
echo
"<tr>
<td class='profile_image'><img src='{$value->profile_image_url_https}'></td>
<td class='name'>{$value->name}<br><a href='http://twitter.com/{$value->screen_name}'>@{$value->screen_name}</a></td>
<td class='following'><a href='http://twitter.com/{$value->screen_name}/following'>{$value->friends_count}</a></td>
<td class='followers'><a href='http://twitter.com/{$value->screen_name}/followers'>{$value->followers_count}</a></td>
</tr>";
}
echo "</table>";

/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham')));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992)));
//$connection->post('friendships/destroy', array('id' => 9436992)));
