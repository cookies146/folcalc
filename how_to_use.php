<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>使い方 - Follow Calculator(仮)</title>
<style type="text/css">
<!--
div p { padding-left: 10px;}
-->
</style>
</head>

<body>
このアプリケーションの使い方を簡単に書こうかと。<br>
とはいえ、ちょっと誰にでもわかりやすく書くのは難しいんですよねぇ。<br>
<div><h3>目次</h3>
初めのほうは飛ばして<a href='#expression_format'>計算式の形式</a>から読むのもありだと思います。<br>

<a href='#what_is_folcalc'>概要</a><br>
<a href='#how_to_calculate'>計算実行方法</a><br>
<a href='#expression_format'>計算式の形式</a><br>
<a href='#expression_example'>計算式の例</a><br>
<a href='#saigoni'>最後に</a><br>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
では。
<hr>
<div><a name='what_is_folcalc' href='#what_is_folcalc'>概要</a><br>
<p>
まず、このアプリケーションは（自分以外でも）ある人が「フォローしている人」「フォローされている人」を調べ、<br>
それをもとに簡単な計算を実行できるようにしてみた、ってものです。<br>
</p>
</div>
<br>
<div><a name='how_to_signin' href='#how_to_calculate'>計算実行方法</a><br>
<p>
まず<a href='/twitter/folcalc/'>アプリケーショントップ</a>から、Sign in with twitterでサインインします。<br>
Cookiesを信頼できるようなら、出てきたページでログインしてください。<br>
その後、計算式を<a href='/twitter/folcalc/'>アプリケーショントップ</a>の左のほうにあるインプットボックスに入力してから「調べる」ボタンを押すことで計算を実行できます。
</p>
</div>
<br>
<div><a name='expression_format' href='#expression_format'>計算式の形式</a><br>
<p>
計算式は、「項」と「演算子」、「カッコ」からなります。ここは普段の数学の多項式と同じです。<br>
今現在使える演算子は「AND」「OR」「XOR」の三つです。<br>
<br>
ANDは共に含まれるもの、ORはいずれか一方だけでも含まれるもの、XORはいずれか一方のみ含まれるものを計算します。<br>
<br>
<br>
項は「ユーザ名」と「要素名」からなります。（要素名って言い方も変かもしれませんが。）<br>
ユーザ名は「@[ユーザ名]」という形です。<br>
要素名は「following」「followers」が今のところ使えます。<br>
ユーザ名と要素名は「/」(スラッシュ)で区切られます。<br>
<br>
「@cookies146/following」は「@cookies146がフォローしている人」の集合、<br>
「@cookies146/followers」は「@cookies146をフォローしている人」の集合です。<br>
<br>
<br>
カッコによって計算する順序を変えられます。<br>
まあ、なんとなくわかるでしょうから、この説明は割愛します。<br>
</p>
</div>
<br>
<br>
<br>
<div><a name='expression_example' href='#expression_example'>計算式の例</a><br>
<p>
たとえば「@cookies146」と「@cooooookies1」の両方にフォローされている人を調べたい場合<br>
<b>@cookies146/following AND @cooooookies1/following</b><br>
という風に書けます。<br>
<br>
「@cookies146」にフォローされているか、もしくはフォローしているか、もしくは両方に当てはまる人を調べたい場合<br>
<b>@cookies146/following OR @cookies146/followers</b><br>
です。
<br>
「@cookies146」にフォローされているか、フォローしているか、どちらか一方の場合（片思い・片思われのどちらか）<br>
<b>@cookies146/following XOR @cookies146/followers</b><br>
です。
<br>
</p>
</div>
<br>
<br>
<div><a name='saigoni' href='#saigoni'>最後に</a><br>
<p>
まだ今は「とりあえず」の段階です。テスト段階、とも言えます。<br>
見てのとおり、アプリケーションのほうも使い方のほうも間に合わせの状態です。<br>
今後はアプリケーションのほうにフォローする/リムーブする機能をつけたり、<br>
演算子も多くしたり、もっと見栄えを良くしたりする予定です。<br>
<br>
が、どうぞ追加してほしい機能、このページの改善、質問等ありましたら遠慮なくおっしゃってください。<br>
その際は「@cookies146」に@つきでツイートしてください。<br>
できるだけ、できるだけですが対応していきますから。<br>
</p>
<hr>

</body>
</html>