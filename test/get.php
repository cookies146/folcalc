
<?php
/* Load required lib files. */
session_start();
chdir('../');
require_once('../twitteroauth/twitteroauth.php');
require_once('../config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ../../clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

$content = $connection->get('help/test');
if($content!="ok"){
	if($connection->http_code!=200){
		die("Connection Error:".__LINE__.$connection->http_code);
	}else{
		die("Unknown Error.".__LINE__);
	}
}
/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/rate_limit_status');
print_r($content);
echo "Current API hits remaining: {$content->remaining_hits}.<br>";

require_once("./calculate.inc");
calc_exec($connection, "@cookies146/following AND @re4k/following");