<?php
set_time_limit(180);
/* Load required lib files. */
session_start();
require_once('/www/users/cookies/public_html/twitter/login/twitteroauth/twitteroauth.php');
require_once('/www/users/cookies/public_html/twitter/login/config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: /twitter/login/clearsessions.php?done=/twitter/folcalc/');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

$content = $connection->get('help/test');
if($content!="ok"){
	if($connection->http_code!=200){
		die("Connection Error:".__LINE__.$connection->http_code);
	}else{
		die("Unknown Error.".__LINE__);
	}
}

echo <<<HTML
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>結果</title>
</head>

<body>
HTML;

/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/rate_limit_status');
echo "Current API hits remaining: {$content->remaining_hits}.<br>";

/* Get logged in user to help with tests. */
$user = $connection->get('account/verify_credentials');

/* If method is set change API call made. Test is called by default. */
if(!isset($_POST['expression'])||$_POST['expression']==""){
	echo "式が入力されていません。<a href='index.php'>戻る</a>";
	exit();
}
require_once("./calculate.inc");
if(!calc_init($_POST['expression'])){
	echo "式の文法エラー";
	exit();
}

echo "<table border='1'>";
echo "<tr bgcolor='silver'>
<th>アイコン</td>
<th>名前<br>ユーザ名</td>
<th>following</td>
<th>followers</td>
</tr>";
$res=calc_exec($connection, $_POST['expression']);
foreach($res as $value){
echo
"<tr>
<td class='profile_image'><img src='{$value->profile_image_url_https}'></td>
<td class='name'>{$value->name}<br><a href='http://twitter.com/{$value->screen_name}'>@{$value->screen_name}</a></td>
<td class='following'><a href='http://twitter.com/{$value->screen_name}/following'>{$value->friends_count}</a></td>
<td class='followers'><a href='http://twitter.com/{$value->screen_name}/followers'>{$value->followers_count}</a></td>
</tr>";
}
echo "</table>";
echo <<<HTML
</body>
</html>
HTML;
?>